<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TODO</title>

        <link rel="stylesheet" href="/css/app.bundle.css">

    </head>
    <body class="antialiased">
        <div id="app"></div>
        <script src="/js/app.bundle.js"></script>

    </body>
</html>
