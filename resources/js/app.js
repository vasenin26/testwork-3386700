require('./bootstrap');
window.Vue = require('vue').default;

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

let app = require('./components/TodoList').default;

new Vue({
    el: '#app',
    render: h => h(app)
});
